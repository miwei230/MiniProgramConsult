//app.js
import wxRequest from 'service/wxRequest'
import layer from 'utils/layer'
App({
  /**
   * @var Object 全局变量
   */
  globalData: {},
  /**
   * @var http WxRequest
   * @method get请求 Promise getRequest(url, param)
   * @method post请求 Promise postRequest(url, param)
   */
  http:null,
  /**
   * @var layer Layer
   * @method Promise showError(msg)
   * @method Promise showSuccess(msg)
   * @method Promise showWarn(msg)
   * @method Promise showTips(msg)
   */
  layer:null,
  /**
   * 启动小程序的 query 参数
   * @val Object
   */
  query: null,
  onLaunch: function () {
    //1、自定义网络请求
    this.registerRequest();
    //2、自定义消息弹出层
    this.registerLayer();
    //3.解析入口参数
    this.analysisOptions();
  },
  /**
   * 容器设计模式
   * 把wxRequest注册到app容器中；
   */
  registerRequest(){
    this.http = wxRequest ;
  },
  registerLayer(){
    this.layer = layer;
  },
  analysisOptions(){
    let options = wx.getLaunchOptionsSync().query;
    if (options.scene) {
      /*解析扫码启动参数*/
      var sceneObj = {};
      var scene = decodeURIComponent(options.scene);
      (scene.match(/([\w\-.]*)=([^&]*)/g) || []).forEach((str) => {
        var tmp = str.split('=');
        sceneObj[tmp[0]] = tmp[1];
      })
      delete options.scene;
      options.__proto__ = sceneObj;//修改options原型链
    }

    this.query = options;   //把启动参数挂载到 app.query属性中
  }
})