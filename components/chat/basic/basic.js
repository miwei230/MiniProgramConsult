// components/chat/basic/basic.js
import { toDate, diffSec, formatDiff} from '../../../utils/date.js'
Component({
	options:{
    multipleSlots: true,
		styleIsolation: 'shared'  
	},
  /**
   * 组件的属性列表
   */
  properties: {
	role: String,
	headimg: String,
  time: String,
  },

  /**
   * 组件的初始数据
   */
  data: {
	role: '',
	headimg: '',
  time:'',
  format_date: '刚刚'
  },

  /**
   * 组件的方法列表
   */
  methods: {

  },
  lifetimes:{
    ready(){
      this.setData({
        format_date: formatDiff(diffSec(toDate(this.data.time)))
      })
    }
  }
})
