// components/chat/goods/goods.js
Component({
	options: {
		multipleSlots: true,
		styleIsolation: 'shared',
	},
  /**
   * 组件的属性列表
   */
  properties: {
	role: String,
 	headimg: String,
 	content: Object,
  time: String,
  },

  /**
   * 组件的初始数据
   */
 data: {
 	role: '',
 	headimg: '',
 	content: null,
  time:""
 },

  /**
   * 组件的方法列表
   */
  methods: {

  },
  lifetimes:{
	  ready(){
		 
	  }
  }
})
