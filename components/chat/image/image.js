// components/chat/image/image.js
Component({
	options: {
		styleIsolation: 'shared'
	},
  /**
   * 组件的属性列表
   */
  properties: {
	content: Object,
	headimg: String,
	role: String,
  time: String
  },
  /**
   * 组件的初始数据
   */
  data: {
  content:null,
  time: '',
	role: ''
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
