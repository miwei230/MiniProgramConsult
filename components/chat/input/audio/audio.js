// components/chat/input/audio/audio.js
const recorderManager = wx.getRecorderManager() , app = getApp();
Component({
	options:{
		styleIsolation: 'shared'
	},
	/**
	 * 组件的属性列表
	 */
	properties: {

	},

	/**
	 * 组件的初始数据
	 */
	data: {

	},

	/**
	 * 组件的方法列表
	 */
	methods: {
		changeInputToText(e) {
			this.triggerEvent('changeInputToText');
		},
		startRecoredAudio(e) {
			let pos = {
				y: e.changedTouches[0].pageY,
				timeStamp: e.timeStamp
			}
			this.setData({
				isRecord: true,
				isSend: false,
				pos: pos
			})
			recorderManager.start({
				format: 'mp3'
			})
		},
		endRecoredAudio(e) {
			recorderManager.stop();
			let npos = {
					y: e.changedTouches[0].pageY,
					timeStamp: e.timeStamp
				},
				pos = this.data.pos;
			this.setData({
				isRecord: false,
			})
			if (pos.y - npos.y > 14) {
				return app.layer.showTips('松开手指,取消录音');
			}
			if (npos.timeStamp - pos.timeStamp < 1000) {
				return app.layer.showWarn('说话时间太短');
			}
			this.setData({
				isSend: true
			})
			app.layer.showSuccess('录音成功');
		},
		setRecorderManagerOptions() {
			recorderManager.onStart((e) => {
				console.log('开始录音:', e)
			})
			recorderManager.onStop((options) => {
				if (this.data.isSend == false) return;
				let data = {
					src:options.tempFilePath,
					sec: Math.ceil(options.duration / 1000)
				};
				
				//这里todo 上传文件操作
				this.triggerEvent('sendAudioMessage', data);
			})
			recorderManager.onError((e) => {
				// app.layer.showError(e.errMsg)
				console.log(e.errMsg)
			})
	
		}
	},
	lifetimes: {
		ready() {
			this.setRecorderManagerOptions()
		}
	}
	
	
})
