// components/chat/consult/consult.js
Component({
	options: {
		styleIsolation: 'shared'
	},
	/**
	 * 组件的属性列表
	 */
	properties: {
		
	},

	/**
	 * 组件的初始数据
	 */
	data: {
		isSend: false,
		price: '99',
		src: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big107000.jpg',
		title: '智能AI无线蓝牙音箱！！！'
	},

	/**
	 * 组件的方法列表
	 */
	methods: {
		sendGoodsMessage(e) {
			this.triggerEvent('sendGoodsMessage', {
				price: this.data.price,
				src: this.data.src,
				title: this.data.title
			})
			this.setData({
				isSend: true,
			})
		}
	}
})
