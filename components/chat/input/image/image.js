// components/chat/input/image/image.js
Component({
	options: {
		styleIsolation: 'shared'
	},
	/**
	 * 组件的属性列表
	 */
	properties: {

	},

	/**
	 * 组件的初始数据
	 */
	data: {

	},

	/**
	 * 组件的方法列表
	 */
	methods: {
		choiceImage() {
			let pro = new Promise((resolve, reject) => {
				wx.chooseImage({
					count: 5,
					sizeType: ['compressed'], //可以指定是原图还是压缩图，默认二者都有
					success(res) {
						resolve(res.tempFilePaths)
					},
					fail(res) {
						console.log('选择图片失败!')
						reject(res);
					}
				});
			})

			pro.then((tempFilePaths) => {
				tempFilePaths.forEach((file) => {
					let remoteFile = uploadImage(file);
					this.triggerEvent('sendImageMessage', {
						src: remoteFile
					}); //一张图片为条信息
				})
			})


		}
	}
})

function uploadImage(file) {
	//todo 这里模拟上传图片操作
	return file;
}
