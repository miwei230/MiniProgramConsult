// components/chat/input/input.js
let app = getApp();
Component({
	options: {
		styleIsolation: 'shared'
	},
	/**
	 * 组件的属性列表
	 */
	properties: {

	},

	/**
	 * 组件的初始数据
	 */
	data: {
		type: 'text', //['text', 'audio']
		isRecord: false,
		isSend: false,
		texts: '',
	},

	/**
	 * 组件的方法列表
	 */
	methods: {
		changeInputToText(e) {
			console.log(e)
			this.setData({
				type: 'text'
			})
		},
		changeInputToAudio(e) {
			this.setData({
				type: 'audio',

			})
		},
		inputTexts(e) {
			this.setData({
				texts: e.detail.value
			})
		},
		sendGoodsMessage(e) {
			let data = {
        content: e.detail,
			  type : 'goods'
      };
			this.triggerEvent('sendMessage', data)
		},
		sendAudioMessage(e) {
      let data = {
        content: e.detail,
        type: 'audio'
      };
			this.triggerEvent('sendMessage', data)
		},
		sendImageMessage(e){
        let data = {
          content: e.detail,
          type: 'image'
        };
			this.triggerEvent('sendMessage', data)
		},
		sendTextMessage() {
			try {
				validate(this.data.texts)
			} catch (e) {
				return app.layer.showTips(e);
			}
			this.triggerEvent('sendMessage', {
				content: {
          text: this.data.texts
          },
				type: 'text',
			})
			this.setData({
				'texts': ''
			})
		}
	},
	lifetimes: {
		ready() {

		}
	}

})

function validate(texts) {
	if (texts.length < 5) {
		throw "请输入5个以上的字符!"
	}
	if (texts.length > 255) {
		throw "最多输入255个字符!"
	}
}
