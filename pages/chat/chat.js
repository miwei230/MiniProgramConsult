// pages/me/msg/chat/chat.js
let selfheadimg =  'http://wx.qlogo.cn/mmopen/vi_32/sTiameXobLwBxLK9iav8Uz5JTNiaf9WW4Fpy3vZItOGE1owmicvIxatdTunuTGLs8zZR1icQ98r3r7R0ibgVWy5DeCNA/132'  ;
let stime = new Date().getTime();
Page({

  /**
   * 页面的初始数据
   */
  data: {

	messages: [
		{
			type: 'audio',
			headimg: selfheadimg,
			content: {
        src: 'https://m3.8js.net/2016n/26/99463.mp3'
      },
      time: stime
		},
    {
      type: 'text',
      headimg: selfheadimg,
      content: {
        text: '卧似一张弓'
      }
      , time: stime
    },
    {
      type: 'image',
      headimg: selfheadimg,
      content: {
        src: 'https://skyland.work/upload/18/b03236bc4eed4705ca8c457502b4de.jpg'
      },
      time: stime
    },
	]
  },
  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  /**
   * 发送消息
   */
  sendMessage(e){
	  let messages = this.data.messages,
	  data = e.detail;
	  data.role = "self";
	  data.headimg = selfheadimg,
    data.time = new Date().getTime();
	  messages.push(data);
	  this.setData({
		 messages: messages
	  })
	  this.reponse();
  },
  //模拟回复
   reponse(){
  	let messages = this.data.messages,
	contents = [
		'您好，我现在有事不在，一会再和您联系。',
		'敖喔!敖喔!敖喔!敖喔!敖喔!敖喔!敖喔!敖喔!敖喔!敖喔!',
		'你慢慢说，别急……',
		'你好，我是主人的秘书，有什么事就跟我说吧，等他回来我会转告他的'
	];
	
  	messages.push({
		headimg: selfheadimg,
		role: 'other',
		type: 'text',
		content: {
    text: contents[Math.floor(Math.random() * contents.length )] 
    }
	});
  	this.setData({
		 messages: messages
  	})
     wx.pageScrollTo({
       scrollTop: 99999999,
     });
  }
})

