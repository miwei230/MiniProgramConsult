## 微信小程序 · 商品咨询聊天组件
该组件是从个人的实际项目中抽离出来的，原本的聊天页面是基于colorUI来做的，但其功能仅仅只是一个展示，交互不够理想；故此增加了以下几个功能：
* 商品图文消息
* 图片消息
* 语音聊天（采用InnerAudioContext API实现；微信内置API还不够完善，各型号手机兼容都有问题）

  演示图片（图片部分失帧）http://sanniuben.com/static/images/chat.gif  
  ![部分失帧](http://sanniuben.com/static/images/chat.gif)


### 框架设计
* 这是我们使用的原生小程序开发框架，有兴趣的小伙伴可以了解下
~~~
目录
├─assets         该目录放置所有的静态资源；一般包含图片、公用样式、字体包等
│  ├─images     图片目录
│  ├─styles     公共样式库
│  ├─templates  公共模板           
│  │└─ ...             
├─components    组件目录
│  ├─ ...      
├─config        配置目录
├─pages         页面目录
│   ├─index     主入口必须是index
│       ├─index.js 文件名必须与目录名保持一到
│       ├─index.wxss
│       ├─index.json
│       ├─index.wxml
│       ├─notice 假设,首页中一项公告显示功能，点击则进入公告详情
│           ├─notice.js
│           ├─notice.wxss
│           ├─notice.json
│           └─notice.wxml
│    │└─ ...
|   ├─common 公共页面
         ├─group 假设有一个拼团页面,它既在首页出现，又在商品详情页面出现。那么它就是一个公共页面
            ├─group.js
│           ├─group.wxss
│           ├─group.json
│           └─group.wxml
├─service       服务类目录
├─utils         工具类目录
├─vendor [可选] UI框架目录
    ├─colorui colorUI
    ├─weui    WeUI
    ├─wux-weapp Wux WeApp
~~~
  * service服务目录与utils工具目录区分最简单的方法为：utils的每一个文件都只有一项能力。而service而多个文件组成一个功能
  
### 基础规范
~~~
禁止一切中文及中文字母命名
文件命名统一采用小写方式，以下划线分隔;eg: headimg.jpg,order_list.wxml
目录命名统一采用小写加下划线命名：eg:order_detail
变量必须采用骆驼峰的命名且首字母小写;eg:isHotelBeijing
常量必须采用全大写的命名，且单词以_分割，常量通常用于ajax请求url，和一些不会改变的数据;eg:HOTEL_GET_URL
类必须采用骆驼峰的命名且首字母大写;eg:FooAndToo 
类属性命名，可以使用骆驼峰命名也可以使用下划线命名，但一个类中不能同时出现两种命名风格
所有类方法，必须以骆驼峰小写驼峰命名，且词性必须是动名词或动名词短语；eg:buyGoods,eatFood
事件处理方法命名：on+Action, eg:onChangeNum, onShowMadal
跳转方法命名：go+PageName , eg:goOrderList, goOrderDetail,goGoodsDetail

        
    
   