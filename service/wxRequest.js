import WxRequest from 'wx-request/index';
import Config from '../config/env' 
let wxRequest = new WxRequest({
  baseURL: Config.baseURL,
})
//自定义请求拦截器
wxRequest.interceptors.use({
    request(request) {
      wx.showLoading({
        title: '加载中',
      })
      return request
    },
    requestError(requestError) {
      wx.hideLoading()
      return Promise.reject(requestError)
    },
    response(response) {
      wx.hideLoading()
      return response
    },
    responseError(responseError) {
      wx.hideLoading()
      return Promise.reject(responseError)
    },
  })

/**
 * class WxRequest 
 * @method Promise getRequest(url, config)
 * @method Promise postRequest(url, config)
 * @method Promise putRequest(url, config)
 * @method Promise deleteRequest(url, config)
 * @method Promise traceRequest(url, config)
 * @method Promise optionsRequest(url, config)
 * @method Promise connectRequest(url, config)
 * @method Promise headRequest(url, config)
 */
  export default wxRequest;