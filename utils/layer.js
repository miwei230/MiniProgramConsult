/**
 * 对wx弹出弹进行二次封装
 */
class Layer {
  /**
   * 错误提示
   * @param string msg
   * @return Promise
   */
  showError(msg) {
    return new Promise((resolve, reject) => {
      wx.showModal({
        title: '温馨提示',
        content: msg,
        showCancel: false,
        success: function (res) {
          return resolve(res);
        },

      })
    })
  }
  /**
   * 成功提示
   * @param string msg
   * @return Promise
   */
  showSuccess(msg) {
    return new Promise((resolve, reject) => {
      wx.showToast({
        title: msg,
        image: '/assets/images/success.png',
        success: function (res) {
          setTimeout(function () {
            resolve(res);
          }, 1500);
        }
      });
    });
  }
  /**
   * @param string msg
   * @return Promise
   */
  showWarn(msg) {
    return new Promise((resolve, reject) => {
      wx.showToast({
        title: msg,
        image: '/assets/images/warning.png',
        success: function () {
          setTimeout(function () {
            resolve();
          }, 1500);
        }

      });
    })


  }
  /**
   * 文字提示
   * @param string msg
   * @return Promise
   */
  showTips(msg) {
    return new Promise((resolve, reject) => {
      wx.showToast({
        title: msg,
        icon: 'none',
        success() {
          resolve()
        }
      })
    })
  }
}
export default new Layer